package controllers

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/segmentio/kafka-go"
	_ "github.com/segmentio/kafka-go/snappy"
)

const (
	KafkaConsumerGroup = "consumer-group-objcontrol-devices"
	//	KafkaConsumerGroup = "consumer-group-objcontrol-step"
	KafkaTopic = "pantabasemgo.pantabase-serv.pantahub_steps"
	//	KafkaTopic         = "pantabasemgo.pantabase-serv.pantahub_devices"
)

type StepProcessor struct{}

func (s *StepProcessor) HandleMessage(m kafka.Message) error {

	fmt.Printf("message at topic/partition/offset %v/%v/%v: %s = %s\n", m.Topic, m.Partition, m.Offset, string(m.Key), string(m.Value))
	return nil
}

func (s *StepProcessor) Run() {
	l := log.New(os.Stdout, "kafka-logger: ", 0)
	e := log.New(os.Stdout, "kafka-error: ", 0)
	// make a new reader that consumes from topic-A
	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers:         []string{"kafka-1.datapods.kafka.svc.cluster.local:9092"},
		GroupID:         KafkaConsumerGroup,
		Topic:           KafkaTopic,
		MinBytes:        10e3, // 10KB
		MaxBytes:        10e6, // 10MB
		Logger:          kafka.LoggerFunc(l.Printf),
		ErrorLogger:     kafka.LoggerFunc(e.Printf),
		MaxWait:         1 * time.Second, // Maximum amount of time to wait for new data to come when fetching batches of messages from kafka.
		ReadLagInterval: -1,
	})

	for {
		m, err := r.ReadMessage(context.Background())
		if err != nil {
			break
		}
		s.HandleMessage(m)
	}
	r.Close()
}

func NewStepProcessor() KafkaTopicController {
	return &StepProcessor{}
}
