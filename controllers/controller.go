package controllers

import "github.com/segmentio/kafka-go"

type KafkaTopicController interface {
	HandleMessage(msg kafka.Message) error
	Run()
}
