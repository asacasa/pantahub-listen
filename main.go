package main

import "gitlab.com/pantacor/pantahub-listen/controllers"

func main() {
	ns := controllers.NewStepProcessor()
	ns.Run()
}
